# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and [React Bootstrap](https://react-bootstrap.netlify.app/)

## Available Scripts

In the project directory, you can run:

### `cd aaxis_fed`

### `npm install`

### `npm run dev`


After that you can see the application running in the browser

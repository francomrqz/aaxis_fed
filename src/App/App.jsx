import "./App.css";
import CardCurrency from "../components/CardCurrency";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const App = () => {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <CardCurrency></CardCurrency>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default App;

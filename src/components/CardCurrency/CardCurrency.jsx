import { useEffect, useState } from "react";
import { getCurrency } from "../../services/getCurrency";
import { sendCurrency } from "../../services/sendCurrency";
import Form from "react-bootstrap/Form";

import Row from "react-bootstrap/Row";
import Badge from "react-bootstrap/Badge";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import CardResult from "../CardResult";

const CardCurrency = () => {
  const [currencies, setCurrency] = useState([]);
  const [currencyOrigin, setCurrencyOrigin] = useState("");
  const [currencyDestiny, setCurrencyDestiny] = useState([]);
  const [isLimitOfCurrency, setLimitOfCurrency] = useState(false);
  const [amount, setAmount] = useState(0);
  const [results, calculateResult] = useState([]);

  useEffect(() => {
    const fetchCurrencies = async () => {
      const { data } = await getCurrency();

      let mapCurrencies = [];
      for (const key in data) {
        const element = data[key];
        mapCurrencies.push({
          name: element.name,
          code: element.code,
        });
      }

      setCurrency(mapCurrencies);
    };
    fetchCurrencies();
  }, []);

  
  const selectOrigin = (value) => {
    const code = value.target.value;
    setCurrencyOrigin(code);
  };

  const selectDestiniy = (value) => {
    const code = value.target.value;
    if (currencyDestiny.length < 2){
      const currenciesDestiny = [...currencyDestiny, code];
      setCurrencyDestiny(currenciesDestiny);
    } else {
      setLimitOfCurrency(true)
    }
  };

  const joinCurrency = () => {
    return currencyDestiny.join(", ");
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const currenciesDestiny = currencyDestiny.join();
    const res = await sendCurrency(currencyOrigin, currenciesDestiny);

    calculateResult([])
    const listOfCodes =  Object.entries(res.data)
    for (let index = 0; index < listOfCodes.length; index++) {
      const codeCurrency = listOfCodes[index][0];
      const currencyValue = listOfCodes[index][1];
      
      
      const newResult = {
        currencyDestiny: codeCurrency,
        valueConverted: Number(currencyValue) * Number(amount),
      }
      calculateResult(results => [...results, newResult])
    }
  };

  const renderElement = () => {
    if (isLimitOfCurrency) {
      return <Badge>You have reached the limit of currencies</Badge>
    }
    return null;
  }

  const resetForm = () => {
    calculateResult([])
    setCurrencyDestiny([])
    setLimitOfCurrency(false)
  }

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Row className="mt-4">
          <Col xs={12} md={6}>
            <Form.Label htmlFor="amount">Ammount to convert</Form.Label>
            <Form.Control
              type="text"
              value={amount} onChange={e => setAmount(e.target.value)} 
              id="amount"
              aria-describedby="passwordHelpBlock"
            />
          </Col>
        </Row>

        <Row>
          <Col xs={12} md={6}>
            <Form.Label className="my-3">
              Select a currency to convert
            </Form.Label>
            <Form.Select
              aria-label="Default select example"
              onChange={($event) => selectOrigin($event)}
            >
              {currencies.map((child, index) => (
                <option key={index} value={child.code}>{child.name}</option>
              ))}
            </Form.Select>
          </Col>
          <Col xs={12} md={6}>
            <Form.Label className="my-3">Select a destiny currency</Form.Label>
            <Form.Select
            className="mb-1"
              aria-label="Default select example"
              onChange={($event) => selectDestiniy($event)}
            >
              {currencies.map((child, index) => (
                <option key={index} value={child.code}>{child.name}</option>
              ))}
            </Form.Select>
              { renderElement() }
          </Col>
        </Row>
        <Row className="mt-4">
          <Col>
            <Badge  bg="secondary">{ currencyOrigin }</Badge>
          </Col>
          <Col>
            <p>Currencies selected are <b>{joinCurrency() } </b></p>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col align="end">
            <Button type="submit" className="mx-2">Convert currency</Button>
            <Button type="button" variant="warning"   onClick={() => resetForm()}>Reset form</Button>
          </Col>
        </Row>
      </Form>
      <CardResult results={results} />
    </div>
  );
};

export default CardCurrency;

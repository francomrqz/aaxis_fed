
import Card from "react-bootstrap/Card";  
import Table from 'react-bootstrap/Table';
const cardResult = ({ results }) => {
  
  return (
    <Card className="mt-4">
      <Card.Body>
            <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Currency</th>
          <th>Value converted</th>
        </tr>
      </thead>
       <tbody>
        <tr>
        </tr>
        {results.map((child, index) => (
          <tr>
            <td>{index}</td>
            <td key={index}> {child.currencyDestiny}  </td>
            <td>{child.valueConverted.toFixed(2)}</td>
          </tr>
        ))}
        </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
};

export default cardResult;
